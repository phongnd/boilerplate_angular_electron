import {join} from 'path';
import {format} from 'url';
import initMainProcess from './main-process';
import {BrowserWindow, app} from 'electron';

let mainWindow: any;
let filepath = join(process.resourcesPath, 'app/index.html');

function createWindow() {
    mainWindow = new BrowserWindow({width: 800, height: 650});

    let url = format({
        pathname: filepath,
        protocol: 'file:',
        slashes: true
    });
    mainWindow.loadURL(url);
    mainWindow.webContents.openDevTools();
    mainWindow.on('closed', function () {
        mainWindow = null
    })
}

app.on('ready', createWindow);
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});
app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

initMainProcess();
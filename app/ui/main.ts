import 'reflect-metadata'
import 'core-js/es7/reflect';
import 'core-js/es6';
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";
import {AppModule} from "./app.module";
import { enableProdMode } from '@angular/core';

const process = require("process");
require('zone.js/dist/zone');

if (process.env.NODE_ENV === 'production') {
    enableProdMode();
} else {
    Error['stackTraceLimit'] = Infinity;
    require('zone.js/dist/long-stack-trace-zone');
}

platformBrowserDynamic().bootstrapModule(AppModule);
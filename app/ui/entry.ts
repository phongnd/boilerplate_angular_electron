import 'reflect-metadata';
import {Component, ViewChild} from '@angular/core';
import {Platform, MenuController, Nav} from 'ionic-angular';
import {HelloIonicPage} from './pages/hello-ionic/hello-ionic';
import {ListPage} from './pages/list/list';
import {AppService} from "./services";

@Component({
    template: require('./entry.html')
})
export class Entry {
    @ViewChild(Nav) nav: Nav;

    // make HelloIonicPage the root (or first) page
    rootPage: any = HelloIonicPage;
    pages: Array<{title: string, component: any}>;

    constructor(private platform: Platform,
                private menu: MenuController,
                private _service: AppService) {
        this.initializeApp();

        // set our app's pages
        this.pages = [
            {title: 'Hello Ionic', component: HelloIonicPage},
            {title: 'My First List', component: ListPage}
        ];

        this._service.doSth();
    }

    initializeApp() {
        this.platform.ready().then(() => {
        });
    }

    openPage(page: any) {
        this.menu.close();
        this.nav.setRoot(page.component);
    }
}
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {IonicApp, IonicModule} from 'ionic-angular';
import {Entry} from './entry';
import {HelloIonicPage} from './pages/hello-ionic/hello-ionic'
import {AppService} from './services'

@NgModule({
    declarations: [
        Entry,
        HelloIonicPage,
    ],
    providers: [
        AppService
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        IonicModule.forRoot(Entry, {
            mode: 'md',
            iconMode: 'md'
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        Entry,
        HelloIonicPage
    ]
})
export class AppModule {
}
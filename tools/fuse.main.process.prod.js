const fsbx = require('fuse-box');

const fuseBox = fsbx.FuseBox.init({
    homeDir: 'app',
    outFile: './prod/main.js',
    serverBundle: true,
    shim: {
        electron: { exports: "global.require('electron')" },
    },
    plugins: [
        fsbx.EnvPlugin({ NODE_ENV: "production" }),
        fsbx.TypeScriptHelpers(),
        fsbx.JSONPlugin(),
        fsbx.UglifyJSPlugin()
    ]
}).bundle(">main.prod.ts");
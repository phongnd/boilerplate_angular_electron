# README #

### What is this repository for? ###

* This is the boilerplate to create an electron app using ionic2 and angular
* 1.0

### How do I get set up? ###

* Install dependency 'npm i'
* Start server by running 'npm start'
* Start electron by running 'npm run start:electron'
* Build a production app by running 'npm run build:prod'
* Packing app by using 'npm run pack-*' (* could be mac, linux, win32 or win64)
* There is no test yet. Will add later